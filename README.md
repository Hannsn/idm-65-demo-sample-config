# DEMO config for Forgerock IDM 6.5
based on the samples/sync-with-csv

## clone to idm installation

~~~
git clone https://gitlab.com/Hannsn/idm-65-demo-sample-config.git

~~~

To get a new update from repo just

~~~
cd idm-65-demo-sample-config
git fetch --all && git reset --hard origin/master
~~~

# starting the configuration

example start:

~~~
./startup.sh -p /home/ec2-user/idm-65-demo-sample-config/ -w /home/ec2-user/idm-65-demo-sample-config/
~~~
